import qs from "qs";

class NetworkLayer{
  constructor(url, socketUrl, encryption = false) {
    this.url = url;
  }

  fetchData(params, options) {
    try {
      return this.fetchDataFromUrl(params, options).then(response => response.json()).then(response => {
          console.log("response>>>>>>>>>>>>>>", response);
        if (response.status === "error") {
          /*pass error code for changing the error message at client side @Dipak (15/11/2017)*/
          var errorInfo = response.response.error;
          var error = new Error(errorInfo.message);
          error.code = errorInfo.code;
          throw error;
        }
        return response;
      });
    } catch (e) {
        console.log("e>>>>>>>>>>>>>>", e);
      alert("error caught.." + e);
      return Promise.reject(e);
    }
  }

  fetchDataFromUrl({ uri, body, multipart, encryption, method = "POST" }, options = {}) {
    var url = `${this.url}${uri || ""}`;
    if (method === "POST") {
      var headers = {};
      /*if uploading a  multipart file then do not make headers and modify body @Dipak   */
        headers = {
            Accept: "application/json",
            "Content-Type": "application/x-www-form-urlencoded"
        }
        if (typeof body !== "string") {
            /*in web case urlencoded is used and requires stringify of parameters*/
            if (headers && headers["Content-Type"] === "application/x-www-form-urlencoded") {
                body = qs.stringify(body);
            } else {
                body = JSON.stringify(body);
            }
        }
      var parameters = {
        headers,
        ...options,
        body,
        method
      };
        console.log("parameters>>>>>>>>>>>>>>>>", parameters);
      return fetch(`${url}`, parameters);
    } else {
        console.log("body>>>>>>>>>>>>>>>>", body);
      const encodedQuery = qs.stringify(body);
        console.log("encodedQuery>>>>>>>>>>>>>>>>", encodedQuery);
      return fetch(`${url}?${encodedQuery}`, options);
    }
  }

  setToken(token) {
    this.token = token;
  }
}

export default NetworkLayer;
