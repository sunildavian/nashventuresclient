import NetworkLayer from "./NetworkLayer";
import React, {Component} from "react";
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom'

import Config from "./Config";

class App extends Component {
    render() {
        return (
            <Router>
                <div>
                    <Route exact path="/" component={Home}/>
                    <Route path="/history" component={History}/>
                    <Route path="/history-detail/:searchKey" component={HistoryDetail}/>
                </div>
            </Router>
        );
    }
}

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            isLoading: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({isLoading: true});
        var networkUrl = Config.NETWORK_URL;
        const networkLayer = new NetworkLayer(networkUrl);
        return networkLayer.fetchData({
            uri: "/upload",
            body: {
                search: this.state.value
            }
        }).then(result => {
            alert(result && result.response && result.response.result)
            this.setState({isLoading: false});
        }).catch(err => {
            this.setState({isLoading: false});
            alert("error caught.." + err);
        })
    }


    render() {
        return (
            <div>
                <h1>{this.state.isLoading ? "Saving...." : "Search Images"}</h1>
                <form onSubmit={this.handleSubmit}>
                    <label>
                        Search:
                        <input type="text" value={this.state.value} onChange={this.handleChange}/>
                    </label>
                    <br/>
                    <br/>
                    <input type="submit" value="Submit"/>

                </form>
                <br/>

                <Link to="/history">
                    <button
                        onClick={this.handleClick}>Show search history
                    </button>
                </Link>
            </div>
        );
    }
}

class History extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            isLoading: true
        };
        this.networkLayer = Config.NETWORK_URL;
    }

    componentWillMount() {
        try {
            const networkLayer = new NetworkLayer(this.networkLayer);
            return networkLayer.fetchData({
                uri: "/getImageSearchData"
            }).then(response => {
                var data = response ? response.response : [];
                this.setState({data: data, isLoading: false});
            }).catch((error) => {
                this.setState({isLoading: false})
                alert("error caught.." + error);
            })

        } catch (err) {
            throw err;
        }
    }

    render() {
        if (this.state.isLoading) {
            return <div>Loading...</div>
        }
        var data = this.state.data;
        return (
            <div>
                <ul>
                    {
                        data && data.map((row) => (
                            <li key={row._id}>
                                <Link to={`/history-detail/${row.searchKey}`}>{row.searchKey}</Link>
                            </li>
                        ))
                    }
                </ul>
            </div>
        )

    }
}

class HistoryDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            isLoading: true
        };
        this.networkUrl = Config.NETWORK_URL;
    }

    componentWillMount() {
        try {

            const networkLayer = new NetworkLayer(this.networkUrl);
            return networkLayer.fetchData({
                uri: "/getImageSearchData",
                body: this.props.match && this.props.match.params
            }).then(response => {
                var data = response ? response.response : [];
                this.setState({data: data, isLoading: false});
            }).catch(err => {
                this.setState({isLoading: false});
                alert("error caught.." + err);
            })

        } catch (err) {
            throw err;
        }
    }

    render() {
        if (this.state.isLoading) {
            return <div>Loading...</div>
        }
        var data = this.state.data;
        var files = data && data.length ? data[0].files : [];
        return (
            <div>
                <ul>
                    {
                        files && files.map((row) => (
                            <li key={row.key}>
                                <img src={`${this.networkUrl}/render/${row.key}`}/>
                            </li>
                        ))
                    }
                </ul>
            </div>
        )
    }
}

export default App;